package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "all_coins_table")
public class AllCoins {
    public String walletName;
    public String coinsList;

    @PrimaryKey
    @NonNull
    public int walletId;

    public AllCoins(String walletName, String coinsList, int walletId) {
        this.walletName = walletName;
        this.coinsList = coinsList;
        this.walletId = walletId;
    }
}
