package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;


import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB.AirdropWallet;

import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.Repository.AirdropWalletRepository;

public class AirdropWalletViewModel extends AndroidViewModel {

    private AirdropWalletRepository mRepository;
    private AirdropWallet mList;

    public AirdropWalletViewModel(@NonNull Application application) {
        super(application);
        mRepository = new AirdropWalletRepository(application);
        mList = mRepository.getAllAirdropWallet();
    }

    public AirdropWallet getAllAirdropWallet() {
        return mList;
    }

    public void insertAirdropWallet(AirdropWallet data) {
        mRepository.insertAirdropWallet(data);
    }


}
