package com.ecoinomi.ecoinomiwallet.UI.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ecoinomi.ecoinomiwallet.UI.Services.AirdropWalletFetch;
import com.ecoinomi.ecoinomiwallet.UI.Services.AllCoinsFetch;
import com.ecoinomi.ecoinomiwallet.UI.Services.WalletDataFetch;

public class RefreshServiceReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Local_cache", "Receiver Load");
        Intent walletIntent = new Intent(context, WalletDataFetch.class);
        context.startService(walletIntent);
        Intent coinsIntent = new Intent(context, AllCoinsFetch.class);
        context.startService(coinsIntent);
        Intent airdropIntent = new Intent(context, AirdropWalletFetch.class);
        context.startService(airdropIntent);
    }
}
