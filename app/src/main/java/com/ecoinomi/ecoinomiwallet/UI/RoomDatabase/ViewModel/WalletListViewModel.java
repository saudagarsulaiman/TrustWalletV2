package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;


import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB.WalletList;

import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.Repository.WalletListRepository;

import java.util.List;

public class WalletListViewModel extends AndroidViewModel {

    private WalletListRepository mRepository;
    private LiveData<List<WalletList>> mList;

    public WalletListViewModel(@NonNull Application application) {
        super(application);
        mRepository = new WalletListRepository(application);
        mList = mRepository.getAllWalletList();
    }

    public LiveData<List<WalletList>> getAllWalletList() {
        return mList;
    }

    public void insertWalletList(WalletList data) {
        mRepository.insertWalletList(data);
    }


}
