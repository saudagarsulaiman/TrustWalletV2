package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "account_wallet_table")
public class AccountWallet{

    @PrimaryKey
    @NonNull
    public String walletDatas;


    public AccountWallet(@NonNull String walletDatas) {
        this.walletDatas = walletDatas;
    }


}
