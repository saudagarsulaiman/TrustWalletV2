package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

public interface WalletUIChangeListener {

    void onWalletUIChanged(String wallets,Boolean isDefaultWallet);
    void onWalletCoinUIChanged(String allCoins);
}
