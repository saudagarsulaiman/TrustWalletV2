package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

import com.ecoinomi.ecoinomiwallet.UI.Models.AccountWallet;

public interface FavListener {
    void addOrRemoveFav(AccountWallet accountWallet, int pos);
}
