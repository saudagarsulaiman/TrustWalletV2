package com.ecoinomi.ecoinomiwallet.UI.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecoinomi.ecoinomiwallet.R;

import butterknife.ButterKnife;

public class ExchangeSettingsFragment extends Fragment {


    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.exchange_settings_fragment, container, false);
        ButterKnife.bind(this, view);


        return view;
    }


}
