package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "airdrop_wallet_coins_table")
public class AirdropWallet {
    @PrimaryKey
    @NonNull
    public int id;

    public String airdropWallet;

    public AirdropWallet(int id, String airdropWallet) {
        this.id = id;
        this.airdropWallet= airdropWallet;
    }
}
