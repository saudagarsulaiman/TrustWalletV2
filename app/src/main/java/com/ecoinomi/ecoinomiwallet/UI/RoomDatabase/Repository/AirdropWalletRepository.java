package com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.Repository;

import android.app.Application;
import android.os.AsyncTask;

import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.ModelsRoomDB.AirdropWallet;
import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.Database.DeviantXDB;
import com.ecoinomi.ecoinomiwallet.UI.RoomDatabase.InterfacesDB.AirdropWalletDao;

public class AirdropWalletRepository {

    private AirdropWalletDao airdropWalletDao;
    private AirdropWallet airdropWallet;

    public AirdropWalletRepository(Application application) {
        DeviantXDB db = DeviantXDB.getDatabase(application);
        airdropWalletDao = db.airdropWalletDao();
        airdropWallet = airdropWalletDao.getAllAirdropWallet();
    }


    public AirdropWallet getAllAirdropWallet() {
        return airdropWallet;
    }

    public void insertAirdropWallet(AirdropWallet airdropWallet) {
        new insertAsyncTask(airdropWalletDao).execute(airdropWallet);
    }

    private static class insertAsyncTask extends AsyncTask<AirdropWallet, Void, Void> {

        private AirdropWalletDao mAsyncTaskDao;

        insertAsyncTask(AirdropWalletDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final AirdropWallet... params) {
            mAsyncTaskDao.insertAirdropWallet(params[0]);
            return null;
        }
    }

}
