package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

import com.ecoinomi.ecoinomiwallet.UI.Models.AllCoins;

import java.util.ArrayList;

public interface CoinSelectableListener {
    public void CoinSelected(ArrayList<AllCoins> allCoinsList,int pos);
}
