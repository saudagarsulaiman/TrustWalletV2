package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

public interface AllCoinsUIListener {
    void onChangedAllCoins(String allCoinsList);
}
