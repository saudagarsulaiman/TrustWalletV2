package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

import com.ecoinomi.ecoinomiwallet.UI.Models.WalletList;

import java.util.ArrayList;

public interface WalletSelectableListener {
    public void WalletSelected(ArrayList<WalletList> allWalletsList, int pos);
}
