package com.ecoinomi.ecoinomiwallet.UI.Interfaces;

public interface AirdropWalletUIListener {
    void onChangedAirdropWallet(String airdropWalletList);
}
