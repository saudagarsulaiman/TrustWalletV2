package com.ecoinomi.ecoinomiwallet.Utilities;

public class CONSTANTS {


    public static String DeviantMulti = "DEVIANTMULTI "; //Authentication @API Services
    public static String usrnm = "USERNAME";  //USerName --- @LoginActivity
    public static String email = "EMAIL";  //Email --- @LoginActivity
    public static String pswd = "PASSWORD"; // Password --- @LoginActivity
    public static String wallet = "WALLET"; //Wallet --- @ SetUpWalletActivity
    public static String token = "TOKEN";     //Account token for Authorization @API Services
    public static String TAG = "!!--DeviantX--!!";  // Console/LogCat
    public static String walletName = "WALLETNAME";   // User Wallet Name @ SetUp Wallet Activity
    public static String walletId = "WALLETID";   // User Wallet Id @ SetUp Wallet Activity
    public static String hideBal = "HIDEBALANCE";  //  Hiding Balance @AppSettingsActivity
    public static String screenshot = "SCREENSHOT";  //  Disable Screenshots @AppSettingsActivity
    public static String lightmode = "LIGHTMODE";  //  lIGHT MODE @AppSettingsActivity
    public static String selectedAccountWallet = "SELECTEDACCOUNTWALLET"; // Selected Account Wallet for storing data@ Adapters

    public static String sender = "SENDER"; // Sender @ Adapters
    public static String receiver = "RECEIVER"; // Sender @ Adapters
    public static String total_avail_bal = "TOTALAVAILBAL";
    public static String usdValue = "USDVALUE";
    public static String first_time = "FIRSTTIME";
    public static String allTags = "allTags"; //All Tags (SEEDS)
    public static String selectedTags = "selectedTags"; // Selected Tags (SEEDS)
    public static String seed = "SEED"; // Accound Seed (Recovery Phrase)
    public static String selectedCoin = "SELECTEDCOIN"; // Selected coin from ALL COINS   @ ExploreCoinsRAdapter
    public static String selectedWalletCoin = "SELECTEDWALLETCOIN"; // Selected coin from ALL COINS   @ ExploreCoinsRAdapter
    public static String empty_wallet = "EMPTYWALLET";
    public static String selectedWallet = "SELECTEDWALLET";
    public static String first_wallet = "FIRSTWALLET";//First Time Wallet Creation
    public static String twoFactorAuth = "TWOFACTORAUTHENTICATION";    //Two Factor Authentication
    public static String twoFACode = "2FACODE";
    public static String defaultWallet = "DEFAULTWALLET"; //Default Wallet
    public static String seletedTab = "SELECTEDTAB";
    public static String amount = "AMOUNT";
    public static String address = "ADDRESS";
    public static String coinCode="COINCODE";
    public static String check="CHECK";
    public static String login2FA="2FALOGIN";
    public static String send_bal="SENDBAL";
    public static String fiat_bal="FIATBAl";
    public static String ttl_rcv="TOTALRECEIVE";


//    //    Selected Account Wallet Details
//    public static String data_id="data_id";
//    public static String data_address="data_address";
//    public static String data_walletName="data_walletName";
//    public static String data_privatekey="data_privatekey";
//    public static String data_passcode="data_passcode";
//    public static String data_balance="data_balance";
//    public static String data_balUsd="data_balUsd";
//    public static String data_balInr="data_balInr";
//    public static String data_account="data_account";
//    public static String data_coin_id="data_coin_id";
//    public static String data_coin_name="data_coin_name";
//    public static String data_coin_code="data_coin_code";
//    public static String data_coin_logo="data_coin_logo";
//    public static String data_coin_usdval="data_coin_usdval";
}
